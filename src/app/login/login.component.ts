import { Component, OnInit } from '@angular/core';
import { Employee } from '../employees/employee.model';
import { ServiceService } from '../services/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Token } from '../shared/token';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ServiceService]
})
export class LoginComponent implements OnInit {
    model = new Employee();
    token: Token;
    constructor(private _services: ServiceService, private _router: Router) { }

  ngOnInit() {
  }
  onSubmit() {
      console.log(this.model.Email);
      console.log(this.model.Password);
      this._services.doLogin(this.model.Email, this.model.Password).subscribe((data) => this.token = data);
      console.log(this.token);
      if (this.token) {
          console.log(this.token);
          this._router.navigate(['dashboard']);
      }
  }
}
