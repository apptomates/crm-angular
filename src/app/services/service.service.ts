import { Injectable } from '@angular/core';
import { Employee } from '../employees/employee.model';
import { Http, Headers, Response, RequestMethod, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Token } from '../shared/token';
import { Role } from '../employees/employee.role';
import { Designation } from '../employees/employee.designation';
import { Gender } from '../employees/employee.gender';
import { Leave, vwLeave } from '../lms/lms.model';
import { Type } from '../lms/lms.type';
import { LeaveXREF } from '../lms/lms.leaveXref';


@Injectable()
export class ServiceService {
    constructor(private _http: Http) {

    }
    doLogin(email: string, password: string) {
        return this._http.get("http://localhost:49942/api/Employees/Login?Email=" + email + "&Password=" + password)
            .map((res: Response) => <Token>res.json());
    }

    getEmployees() {
        return this._http.get("http://localhost:49942/api/Employees/GetEmployees")
            .map((response: Response) => <Employee[]>response.json());
    }

    getRole(): Observable<Role[]> {
        return this._http.get("http://localhost:49942/api/Employees/GetRoles")
            .map((response: Response) => <Role[]>response.json())
            
    }

    getDesignation(): Observable<Designation[]> {
        return this._http.get("http://localhost:49942/api/Employees/GetDesignations")
            .map((response: Response) => <Designation[]>response.json())

    }

    getGender(): Observable<Gender[]> {
        return this._http.get("http://localhost:49942/api/Employees/GetGenders")
            .map((response: Response) => <Gender[]>response.json())

    }

    getLeaves() {
        return this._http.get("http://localhost:49942/api/LMS/GetLeavesByEmployeeID?EmployeeID=1013")
            .map((response: Response) => <vwLeave[]>response.json());
    }

    getType(): Observable<Type[]> {
        return this._http.get("http://localhost:49942/api/LMS/GetType")
            .map((response: Response) => <Type[]>response.json())

    }
   

    postEmployee(emp: Employee) {
        console.log(emp);
        var body = JSON.stringify(emp);
        console.log(body);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ headers: headerOptions });
        return this._http.post('http://localhost:49942/api/Employees/UpsertEmployee', body, requestOptions).map(x => x.json());

    }

    putEmployee(id, emp) {

        var body = JSON.stringify(emp);
        console.log(body);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ headers: headerOptions });
        return this._http.post('http://localhost:49942/api/Employees' + id, body, requestOptions).map(res => res.json());

    }

    deleteEmployee(id: number) {
        return this._http.delete('http://localhost:49942/api/Employees/DeleteEmployee?EmployeeID=' + id).map(res => res.json());


    }

    getEmployee(id: number) {
        return this._http.get('http://localhost:49942/api/Employees/GetEmployee?EmployeeID=' + id).map(res => res.json());
            
    }

    postLeave(lev: Leave) {
        console.log(lev);
        var body = JSON.stringify(lev);
        console.log(body);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ headers: headerOptions });
        return this._http.post('http://localhost:49942/api/LMS/UpsertLeave', body, requestOptions).map(x => x.json());

    }

    putLeave(id, lev) {

        var body = JSON.stringify(lev);
        console.log(body);
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ headers: headerOptions });
        return this._http.post('http://localhost:49942/api/LMS/Leaves' + id, body, requestOptions).map(res => res.json());

    }

    deleteLeave(id: number) {
        return this._http.delete('http://localhost:49942/api/LMS/DeleteLeave?LeaveID=' + id).map(res => res.json());


    }

    getAllLeaves(id: number) {
        return this._http.get('http://localhost:49942/api/LMS/GetAllLeaves').map(res => res.json());

    }
    
    approveLeave(id: number) {
        return this._http.delete('http://localhost:49942/api/LMS/ApproveLeave?LeaveID=' + id).map(res => res.json());


    }

    declineLeave(id: number) {
        return this._http.delete('http://localhost:49942/api/LMS/DeclineLeave?LeaveID=' + id).map(res => res.json());


    }

    getLeaveXref(id: number) {
        return this._http.get('http://localhost:49942/api/LMS/GetLeavesXref?EmployeeID=' + id)
            .map((response: Response) => <LeaveXREF[]>response.json());
    }

    

}