import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceService } from '../services/service.service';
import { Leave, vwLeave } from '../lms/lms.model';
import { Type } from '../lms/lms.type';
import { ToastrService } from 'ngx-toastr';
import { LeaveXREF } from '../lms/lms.leaveXref';

@Component({
    selector: 'app-lms',
    templateUrl: './lms.component.html',
    styleUrls: ['./lms.component.scss'],
    providers: [ServiceService]
})
export class LmsComponent implements OnInit {
    Leaves: vwLeave[];
    model = new Leave();
    type: Type[];
    xref: LeaveXREF[];

    constructor(private _services: ServiceService, private toastr: ToastrService) { }

    ngOnInit() {

        this._services.getLeaves().subscribe(data => this.Leaves = data);
        this._services.getType().subscribe(data => this.type = data);
        this._services.getLeaveXref(1007).subscribe(data => this.xref = data);
        console.log(this.Leaves);
    }
    resetForm(form?: NgForm) {
        if (form != null)
            form.reset();

    }
    onSubmit(form: NgForm) {
       
        console.log(form);
        if (form.value.LeaveID == null) {
            form.value.LeaveID = 0;
        }
        this._services.postLeave(form.value)
            .subscribe(data => {
                this._services.getLeaves().subscribe(data => this.Leaves = data);
                this.resetForm(form);
            })

    }

    deleteLeave(LeaveID: number) {
        //console.log(EmployeeID);
        this._services.deleteEmployee(LeaveID).subscribe((data: any) => {
            this._services.getLeaves().subscribe(data => this.Leaves = data);
            this.resetForm();
            this.toastr.success('Leave Deleted Successfully');
        });
    }

    approveLeave(LeaveID: number) {
        //console.log(EmployeeID);

        this._services.approveLeave(LeaveID).subscribe((data: any) => {
            this._services.getLeaves().subscribe(data => this.Leaves = data);
            this.toastr.success('Approved Successfully');
        });

    }
    declineLeave(LeaveID: number) {
        //console.log(EmployeeID);
        this._services.declineLeave(LeaveID).subscribe((data: any) => {
            this._services.getLeaves().subscribe(data => this.Leaves = data);
            this.toastr.success('Declined Successfully');
        });
    }

}