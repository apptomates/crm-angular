﻿export class Leave {
    LeaveID: number;
    Reason: string;
    FromDate: Date;
    ToDate: Date;
    LeaveTypeID: number;
    StatusID: number;
    EmployeeID: number;
}
export class vwLeave extends Leave{
    FirstName: string;
    LeaveStatus: string;
    LeaveType: string;
}
