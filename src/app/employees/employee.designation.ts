﻿export class Designation {
    DesignationID: number;
    DesignationName: string;
    DepartmentID: number;
}