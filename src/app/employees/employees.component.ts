import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from '../employees/employee.model';
import { ServiceService } from '../services/service.service';
import { ToastrService } from 'ngx-toastr';
import { Role } from '../employees/employee.role';
import { Designation } from '../employees/employee.designation';
import { Gender } from '../employees/employee.gender';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
  providers: [ServiceService]
  
})
export class EmployeesComponent implements OnInit {
    employees: Employee[];
    model = new Employee();
    role: Role[];
    desig: Designation[];
    gender: Gender[];
    submitBtn: string;
   

    constructor(private _services: ServiceService, private toastr: ToastrService) { }

    ngOnInit() {
        this.resetForm();
        this._services.getEmployees().subscribe(data => this.employees = data);
        this._services.getRole().subscribe(data => this.role = data);
        this._services.getDesignation().subscribe(data => this.desig = data);
        this._services.getGender().subscribe(data => this.gender = data);
        this.submitBtn = 'Create';
        
  }
    resetForm(form?: NgForm) {
        if (form != null)
            form.reset();
        this.submitBtn = 'Create';

        this.model = {
            EmployeeID: null,
            FirstName: '',
            LastName: '',
            Email: '',
            Password: '',
            PhoneNumber: null,
            RoleID: null,
            DesignationID: null,
            DateOfBirth: null,
            Address: '',
            City: '',
            State: '',
            Country: '',
            Zip: null,
            ProfileImage: '',
            GenderID: null,

        }
    }

    onSubmit(form: NgForm) {
        console.log(form);
        if (form.value.EmployeeID == null) {
            form.value.EmployeeID = 0;
        }
            this._services.postEmployee(form.value)
                .subscribe(data => {
                    this._services.getEmployees().subscribe(data => this.employees = data);
                    
                    if (this.submitBtn == "Create") {
                        this.toastr.success('Employee Added Successfully');
                    }
                    else {
                        this.toastr.success('Employee Updated Successfully');
                    }
                    this.resetForm(form);
                    
                   
                })
       
    }

    deleteEmployee(EmployeeID: number) {
        console.log(EmployeeID);
        this._services.deleteEmployee(EmployeeID).subscribe((data: any) => {
            this._services.getEmployees().subscribe(data => this.employees = data);
            this.resetForm();
            this.toastr.success('Employee Deleted Successfully');
        });
    }

    getEmployee(EmployeeID: number) {
        console.log(EmployeeID);
        this._services.getEmployee(EmployeeID).subscribe((data: any) => {
            this.model = data;
        });
            
    }

    editEmployee(EmployeeID: number) {
        console.log(EmployeeID);
        this._services.getEmployee(EmployeeID).subscribe((data: any) => {
            this._services.getEmployees().subscribe(data => this.employees = data);
            this.model = data;
            this.submitBtn = 'Update';
            console.log(data);
           
        });
}



}
