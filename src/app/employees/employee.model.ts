export class Employee {
    EmployeeID: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    PhoneNumber: number;
    RoleID: number;
    DesignationID: number;
    DateOfBirth: number;
    Address: string;
    City: string;
    State: string;
    Country: string;
    Zip: number;
    ProfileImage: string;
    GenderID: number;
}
